# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/21 17:30:42 by aacuna            #+#    #+#              #
#    Updated: 2016/02/04 18:35:31 by aacuna           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fractol

SRCS = fractol.c		\
	   complex.c		\
	   burning_ship.c	\
	   julia.c			\
	   mandelbrot.c		\
	   events.c			\
	   zoom.c

CFLAGS = -Wall -Wextra -Werror -I libft -I minilibx_macos

LIBRARY = libft/libft.a minilibx_macos/libmlx.a

MINILIBX = -framework OpenGL -framework AppKit

OBJS = $(SRCS:.c=.o)

$(NAME)	:	$(OBJS)
			make -C libft/
			make -C minilibx_macos
			gcc $(CFLAGS) -o $(NAME) $(OBJS) $(LIBRARY) $(MINILIBX)

all		:	$(NAME)

clean	:
			rm -rf $(OBJS)

fclean	:	clean
			rm -rf $(NAME)

re		:	fclean all
