/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burning_ship.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/02 14:30:48 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 19:25:28 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	calculate_color(int i)
{
	return (0x00000000 + (((i * 3) % 256) * 256 * 256) + ((i % 256) * 256));
}

int	burning_explodes(t_complex nb)
{
	return (get_module(nb) > 2);
}

int	calculate_burning_ship(t_complex nb)
{
	int			i;
	t_complex	result;
	t_zoom		*zoom;
	int			max_iterations;

	i = 1;
	zoom = get_zoom();
	max_iterations = 50 + (zoom->level / 2);
	result = nb;
	while (i < max_iterations && !burning_explodes(result))
	{
		result = complex_add(complex_sqr(complex_abs(result)), nb);
		i++;
	}
	if (i == max_iterations)
		return (0x00000000);
	else
		return (calculate_color(i + 10));
}
