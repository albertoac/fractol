/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 15:25:36 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 19:25:35 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <math.h>
# include <stdlib.h>
# include "mlx.h"
# include "libft.h"

# define SIZE_X 400
# define SIZE_Y 400
# define JULIA	1
# define MANDEL	2
# define SHIP	3

typedef struct		s_img
{
	void			*ptr;
	unsigned char	*str;
	int				bpp;
	int				line_size;
	int				endian;
}					t_img;

typedef	struct		s_zoom
{
	double			x_left;
	double			x_right;
	double			y_up;
	double			y_down;
	int				level;
}					t_zoom;

typedef struct		s_complex
{
	double			real;
	double			imaginary;
}					t_complex;

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	t_img			img;
	int				fractal;
}					t_env;

t_complex			complex_sqr(t_complex n);
t_complex			complex_add(t_complex nb1, t_complex nb2);
t_complex			complex_abs(t_complex nb);
t_complex			create_complex(double real, double imaginary);
double				get_module(t_complex nb);
int					calculate_julia(t_complex nb, t_complex constant);
int					calculate_mandelbrot(t_complex nb);
int					calculate_burning_ship(t_complex nb);
void				init_hooks(t_env e);
void				print_fractal(t_complex *c, t_env e);
t_complex			get_transformation(int orig_x, int orig_y);
t_zoom				*get_zoom(void);
void				increase_zoom(int x, int y);
void				decrease_zoom(int x, int y);
void				print_error(void);
t_zoom				*store_zoom(double x_left, double x_right,
								double y_up, double y_down);

#endif
