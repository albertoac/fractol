/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 17:18:28 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/25 10:45:58 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	char	*str1;
	char	*str2;
	size_t	i;
	size_t	j;

	str1 = (char*)s1;
	str2 = (char*)s2;
	i = 0;
	while ((str1[i] != '\0') && (i < n))
	{
		j = 0;
		while ((str1[i + j] == str2[j]) && (str2[j] != '\0') && ((i + j) < n))
			j++;
		if (str2[j] == '\0')
			return (str1 + i);
		i++;
	}
	return (NULL);
}
