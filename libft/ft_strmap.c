/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 14:25:00 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/25 14:35:29 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		length;
	int		i;
	char	*res;

	length = ft_strlen(s);
	res = (char*)malloc((length * sizeof(*res)) + 1);
	if (res == NULL)
		return (NULL);
	i = 0;
	while (i < length)
	{
		res[i] = f(s[i]);
		i++;
	}
	res[i] = '\0';
	return (res);
}
