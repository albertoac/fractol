/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 19:15:22 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/02 10:14:48 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_memmove_backwards(unsigned char *dst, unsigned char *src,
								size_t len)
{
	dst = dst + len - 1;
	src = src + len - 1;
	while (len > 0)
	{
		*dst = *src;
		dst--;
		src--;
		len--;
	}
}

void		*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char	*pdst;
	unsigned char	*psrc;

	pdst = (unsigned char*)dst;
	psrc = (unsigned char*)src;
	if (pdst < psrc)
		while (len > 0)
		{
			*pdst = *psrc;
			pdst++;
			psrc++;
			len--;
		}
	else
	{
		ft_memmove_backwards(pdst, psrc, len);
	}
	return (dst);
}
