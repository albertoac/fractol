/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   complex.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/20 13:52:14 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/02 15:11:15 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_complex	create_complex(double real, double imaginary)
{
	t_complex nb;

	nb.real = real;
	nb.imaginary = imaginary;
	return (nb);
}

t_complex	complex_sqr(t_complex n)
{
	t_complex result;

	result.real = (n.real * n.real) - (n.imaginary * n.imaginary);
	result.imaginary = 2 * n.real * n.imaginary;
	return (result);
}

t_complex	complex_add(t_complex nb1, t_complex nb2)
{
	t_complex result;

	result.real = nb1.real + nb2.real;
	result.imaginary = nb1.imaginary + nb2.imaginary;
	return (result);
}

double		get_module(t_complex nb)
{
	double result;

	result = sqrt((nb.real * nb.real) + (nb.imaginary * nb.imaginary));
	return (result);
}

t_complex	complex_abs(t_complex nb)
{
	if (nb.real < 0)
		nb.real = -1 * nb.real;
	if (nb.imaginary < 0)
		nb.imaginary = -1 * nb.imaginary;
	return (nb);
}
