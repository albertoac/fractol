/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 10:56:57 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 19:25:22 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	eq_axis(double *x_left, double *x_right, double *y_up, double *y_down)
{
	if (*x_left < -2)
		*x_right = *x_right - (*x_left + 2);
	if (*x_right > 2)
		*x_left = *x_left - (*x_right - 2);
	if (*y_up > 2)
		*y_down = *y_down - (*y_up - 2);
	if (*y_down < -2)
		*y_up = *y_up - (*y_down + 2);
}

t_zoom	*store_zoom(double x_left, double x_right, double y_up, double y_down)
{
	static t_zoom zoom = {-2, 2, 2, -2, 1};

	if (x_left != x_right && y_up != y_down)
	{
		eq_axis(&x_left, &x_right, &y_up, &y_down);
		if (x_left >= -2)
			zoom.x_left = x_left;
		else
			zoom.x_left = -2;
		if (x_right <= 2)
			zoom.x_right = x_right;
		else
			zoom.x_right = 2;
		if (y_up <= 2)
			zoom.y_up = y_up;
		else
			zoom.y_up = 2;
		if (y_down >= -2)
			zoom.y_down = y_down;
		else
			zoom.y_down = -2;
	}
	return (&zoom);
}

t_zoom	*get_zoom(void)
{
	return (store_zoom(0, 0, 0, 0));
}

void	increase_zoom(int x, int y)
{
	t_zoom	*zoom;
	double	x_percent;
	double	y_percent;
	double	distance_x;
	double	distance_y;

	zoom = get_zoom();
	x_percent = x / (double)SIZE_X;
	y_percent = y / (double)SIZE_Y;
	distance_x = (zoom->x_right - zoom->x_left) * 0.10;
	distance_y = (zoom->y_up - zoom->y_down) * 0.10;
	zoom->level++;
	store_zoom(zoom->x_left + (distance_x * x_percent),
				zoom->x_right - (distance_x * (1 - x_percent)),
				zoom->y_up - (distance_y * y_percent),
				zoom->y_down + (distance_y * (1 - y_percent)));
}

void	decrease_zoom(int x, int y)
{
	t_zoom	*zoom;
	double	x_percent;
	double	y_percent;
	double	distance_x;
	double	distance_y;

	zoom = get_zoom();
	x_percent = x / (double)SIZE_X;
	y_percent = y / (double)SIZE_Y;
	distance_x = ((zoom->x_right - zoom->x_left) / 0.90) -
				(zoom->x_right - zoom->x_left);
	distance_y = ((zoom->y_up - zoom->y_down) / 0.90) -
				(zoom->y_up - zoom->y_down);
	if (zoom->level > 1)
		zoom->level--;
	store_zoom(zoom->x_left - (distance_x * x_percent),
				zoom->x_right + (distance_x * (1 - x_percent)),
				zoom->y_up + (distance_y * y_percent),
				zoom->y_down - (distance_y * (1 - y_percent)));
}
