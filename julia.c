/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/25 11:04:15 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 18:46:15 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	julia_explodes(t_complex nb)
{
	return (get_module(nb) > 2);
}

int	calculate_julia(t_complex nb, t_complex constant)
{
	int			i;
	t_complex	result;
	t_zoom		*zoom;
	int			max_iterations;

	i = 1;
	zoom = get_zoom();
	max_iterations = 50 + (zoom->level / 2);
	result = complex_add(complex_sqr(nb), constant);
	while (i < max_iterations && !julia_explodes(result))
	{
		result = complex_add(complex_sqr(result), constant);
		i++;
	}
	if (i == max_iterations)
		return (0x00FFFFFF);
	else
		return (0x00FFFFFF * (i / (double)(101)));
}
