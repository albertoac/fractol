# Fractol #

Fractol will only work on Mac computers

### Installation ###

With the terminal, enter in the folder you just downloaded and type
```
#!shell

make

```

If everything is ok, the project will be compiled and a new file called "fractol" will be created.

### Usage ###

To execute the program you just need to run

```
#!shell

./fractol julia
```
You can replace julia by mandelbrot or ship. You can also change the fractal with using the left and right arrow keys. To zoom in the fractal you can use your mouse wheel.

![Screen Shot 2016-09-02 at 12.28.11 PM.png](https://bitbucket.org/repo/7LXrjL/images/2824396770-Screen%20Shot%202016-09-02%20at%2012.28.11%20PM.png)