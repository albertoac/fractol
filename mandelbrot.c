/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:12:15 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 18:46:34 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int	mandelbrot_explodes(t_complex nb)
{
	return (get_module(nb) > 2);
}

int	calculate_mandelbrot(t_complex nb)
{
	int			i;
	t_complex	result;
	t_zoom		*zoom;
	int			max_iterations;

	i = 1;
	zoom = get_zoom();
	max_iterations = 100 + (zoom->level / 2);
	result = nb;
	while (i < max_iterations && !mandelbrot_explodes(result))
	{
		result = complex_add(complex_sqr(result), nb);
		i++;
	}
	if (i == max_iterations)
		return (0x00FFFFFF);
	else
		return (0x00FFFFFF - 0x00FFFFFF * ((i % 100) / (double)(101)));
}
