/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 10:43:28 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 19:25:49 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mouse_hook_move(int x, int y, void *param)
{
	t_complex	constant;
	t_env		*e;

	e = (t_env*)param;
	if (x >= 0 && x <= SIZE_X && y >= 0 && y <= SIZE_Y && e->fractal == JULIA)
	{
		constant = get_transformation(x, y);
		print_fractal(&constant, *e);
	}
	return (1);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == 53)
		exit(0);
	if (keycode == 69)
		increase_zoom(200, 200);
	if (keycode == 124)
	{
		e->fractal = e->fractal + 1;
		if (e->fractal > 3)
			e->fractal = 1;
	}
	if (keycode == 123)
	{
		e->fractal = e->fractal - 1;
		if (e->fractal < 1)
			e->fractal = 3;
	}
	if (keycode == 123 || keycode == 124)
	{
		store_zoom(-2, 2, 2, -2);
		print_fractal(NULL, *e);
	}
	return (0);
}

int		mouse_click(int button, int x, int y, void *param)
{
	t_env	*e;

	e = (t_env*)param;
	if (button == 6 || button == 5)
	{
		if (e->fractal == SHIP)
			increase_zoom(x, SIZE_Y - y);
		else
			increase_zoom(x, y);
		print_fractal(NULL, *e);
	}
	else if (button == 7 || button == 4)
	{
		decrease_zoom(x, y);
		print_fractal(NULL, *e);
	}
	return (0);
}

void	print_error(void)
{
	ft_putstr_fd("USAGE: ./fractol [julia | mandelbrot | ship]", 2);
	exit(0);
}

void	init_hooks(t_env e)
{
	mlx_hook(e.win, 6, 16, mouse_hook_move, &e);
	mlx_mouse_hook(e.win, mouse_click, &e);
	mlx_key_hook(e.win, key_hook, &e);
}
