/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/19 12:13:16 by aacuna            #+#    #+#             */
/*   Updated: 2016/02/04 19:03:42 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		put_pixel_in_img(t_img img, int x, int y, unsigned int color)
{
	int pixel_pos;

	pixel_pos = (y * img.line_size) + (x * 4);
	if (!img.endian)
	{
		img.str[pixel_pos] = color % 256;
		img.str[pixel_pos + 1] = (color / 256) % 256;
		img.str[pixel_pos + 2] = (color / (256 * 256)) % 256;
		img.str[pixel_pos + 3] = (color / (256 * 256 * 256));
	}
	else
	{
		img.str[pixel_pos + 3] = color % 256;
		img.str[pixel_pos + 2] = (color / 256) % 256;
		img.str[pixel_pos + 1] = (color / (256 * 256)) % 256;
		img.str[pixel_pos] = (color / (256 * 256 * 256));
	}
}

t_complex	get_transformation(int orig_x, int orig_y)
{
	double new_point_x;
	double new_point_y;
	t_zoom *limits;

	limits = get_zoom();
	new_point_x = orig_x * (limits->x_right - limits->x_left) / (double)SIZE_X;
	new_point_x = new_point_x + limits->x_left;
	new_point_y = (orig_y * (limits->y_down - limits->y_up) / (double)SIZE_Y);
	new_point_y = new_point_y + limits->y_up;
	return (create_complex(new_point_x, new_point_y));
}

int			select_fractal(int fractal, int x, int y, t_complex constant)
{
	int			color;
	t_complex	nb;

	color = 0;
	nb = get_transformation(x, y);
	if (fractal == JULIA)
		color = calculate_julia(nb, constant);
	else if (fractal == MANDEL)
		color = calculate_mandelbrot(nb);
	else if (fractal == SHIP)
		color = calculate_burning_ship(nb);
	return (color);
}

void		print_fractal(t_complex *c, t_env e)
{
	int					x;
	int					y;
	int					color;
	static t_complex	constant;

	x = 0;
	if (c != NULL)
		constant = *c;
	while (x < SIZE_X)
	{
		y = 0;
		while (y < SIZE_Y)
		{
			color = select_fractal(e.fractal, x, y, constant);
			if (e.fractal == SHIP)
				put_pixel_in_img(e.img, x, SIZE_Y - y, color);
			else
				put_pixel_in_img(e.img, x, y, color);
			y++;
		}
		x++;
	}
	mlx_put_image_to_window(e.mlx, e.win, e.img.ptr, 0, 0);
}

int			main(int argc, char **argv)
{
	t_env		e;
	t_complex	constant;

	if (argc == 2)
	{
		if (ft_strcmp(argv[1], "julia") == 0)
			e.fractal = JULIA;
		else if (ft_strcmp(argv[1], "mandelbrot") == 0)
			e.fractal = MANDEL;
		else if (ft_strcmp(argv[1], "ship") == 0)
			e.fractal = SHIP;
		else
			print_error();
		e.mlx = mlx_init();
		e.win = mlx_new_window(e.mlx, SIZE_X, SIZE_Y, "Fractol");
		e.img.ptr = mlx_new_image(e.mlx, SIZE_X, SIZE_Y);
		e.img.str = (unsigned char*)mlx_get_data_addr(e.img.ptr, &e.img.bpp,
					&e.img.line_size, &e.img.endian);
		constant = create_complex(0, 0);
		print_fractal(&constant, e);
		init_hooks(e);
		mlx_loop(e.mlx);
	}
	print_error();
	return (0);
}
